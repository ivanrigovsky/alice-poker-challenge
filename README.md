# Poker Challenge

### Rules 
http://www.pokerlistings.com/poker-rules-texas-holdem
http://www.pokerlistings.com/texas-holdem-betting-rules

### Bots
Implemented simple always-call strategy. You can add more, if you want via BotController interface. 

### Build and debug
Runner is the main class.  
To debug bot, you can set BOT_DEBUG = true.
You can select number of players (bots and 1 player), big blind amount. 

### Not implemented.
Not finished combinations: Straight, Straight flash. 
