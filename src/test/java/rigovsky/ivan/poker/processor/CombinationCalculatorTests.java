package rigovsky.ivan.poker.processor;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import rigovsky.ivan.poker.model.cards.Card;
import rigovsky.ivan.poker.model.cards.Combination;
import rigovsky.ivan.poker.model.cards.Rank;
import rigovsky.ivan.poker.model.cards.Suit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class CombinationCalculatorTests {

    @Test
    public void highCardCalculationTest() {
        CombinationCalculator calculator = new CombinationCalculator();
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Suit.clubs, Rank.three));
        hand.add(new Card(Suit.diamonds, Rank.jack));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.highCard);
        Integer result = (Rank.jack.getValue() + Rank.three.getValue());
        assertEquals(calculator.getValue(), result);

        hand.add(new Card(Suit.clubs, Rank.king));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.highCard);
        result += Rank.king.getValue();
        assertEquals(calculator.getValue(), result);
    }

    @Test
    public void pairCalculationTest() {
        CombinationCalculator calculator = new CombinationCalculator();
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Suit.clubs, Rank.two));
        hand.add(new Card(Suit.diamonds, Rank.three));
        hand.add(new Card(Suit.spades, Rank.two));

        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.pair);
    }

    @Test
    public void twoPairCalculationTest() {
        CombinationCalculator calculator = new CombinationCalculator();
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Suit.clubs, Rank.two));
        hand.add(new Card(Suit.diamonds, Rank.three));
        hand.add(new Card(Suit.spades, Rank.two));

        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.pair);

        hand.add(new Card(Suit.hearts, Rank.three));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.twoPair);
        Integer oldValue = calculator.getValue();

        hand.add(new Card(Suit.hearts, Rank.four));
        hand.add(new Card(Suit.spades, Rank.four));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.twoPair);
        assertTrue(calculator.getValue() > oldValue);
    }

    @Test
    public void setCalculationTest() {
        CombinationCalculator calculator = new CombinationCalculator();
        List<Card> hand = new ArrayList<>();
        Card threeCard2 = new Card(Suit.spades, Rank.three);
        Card threeCard1 = new Card(Suit.spades, Rank.three);
        hand.add(threeCard1);
        hand.add(threeCard2);
        hand.add(new Card(Suit.diamonds, Rank.four));
        hand.add(new Card(Suit.clubs, Rank.five));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.pair);

        hand.add(new Card(Suit.diamonds, Rank.three));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.set);
        Integer oldValue = calculator.getValue();

        hand.remove(threeCard1);
        hand.remove(threeCard2);
        hand.add(new Card(Suit.spades, Rank.four));
        hand.add(new Card(Suit.spades, Rank.four));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.set);
        assertNotEquals(calculator.getValue(), oldValue);
    }

    @Test
    public void fullHouseCalculationTest() {
        CombinationCalculator calculator = new CombinationCalculator();
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Suit.clubs, Rank.two));
        hand.add(new Card(Suit.spades, Rank.two));
        hand.add(new Card(Suit.hearts, Rank.three));
        hand.add(new Card(Suit.diamonds, Rank.three));
        hand.add(new Card(Suit.hearts, Rank.four));
        hand.add(new Card(Suit.spades, Rank.four));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.twoPair);

        hand.add(new Card(Suit.diamonds, Rank.three));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.fullHouse);
        Integer oldValue = calculator.getValue();

        hand.add(new Card(Suit.diamonds, Rank.two));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.fullHouse);
        assertEquals(calculator.getValue(), oldValue);

        hand.add(new Card(Suit.diamonds, Rank.four));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.fullHouse);
        assertTrue(calculator.getValue() > oldValue);
    }

    @Test
    public void quadsCalculationTest() {
        CombinationCalculator calculator = new CombinationCalculator();
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Suit.clubs, Rank.three));
        hand.add(new Card(Suit.spades, Rank.three));
        hand.add(new Card(Suit.diamonds, Rank.three));
        hand.add(new Card(Suit.clubs, Rank.two));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.set);

        hand.add(new Card(Suit.hearts, Rank.three));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.quads);
        Integer oldValue = calculator.getValue();

        hand.add(new Card(Suit.spades, Rank.two));
        hand.add(new Card(Suit.diamonds, Rank.two));
        hand.add(new Card(Suit.hearts, Rank.two));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.quads);
        assertEquals(calculator.getValue(), oldValue);
    }

    @Test
    public void flushCalculationTest() {
        CombinationCalculator calculator = new CombinationCalculator();
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Suit.clubs, Rank.three));
        hand.add(new Card(Suit.clubs, Rank.four));
        hand.add(new Card(Suit.clubs, Rank.six));
        hand.add(new Card(Suit.clubs, Rank.seven));
        calculator.calculate(hand);
        assertEquals(calculator.getCombinationType(), Combination.highCard);

        hand.add(new Card(Suit.clubs, Rank.five));
        calculator.calculate(hand);
        Integer oldValue = calculator.getValue();
        assertEquals(calculator.getCombinationType(), Combination.flush);

        hand.add(new Card(Suit.clubs, Rank.nine));
        calculator.calculate(hand);
        assertTrue(calculator.getValue() > oldValue);
        assertEquals(calculator.getCombinationType(), Combination.flush);

        oldValue = calculator.getValue();
        hand.add(new Card(Suit.clubs, Rank.two));
        calculator.calculate(hand);
        assertEquals(calculator.getValue(), oldValue);
        assertEquals(calculator.getCombinationType(), Combination.flush);
    }
}
