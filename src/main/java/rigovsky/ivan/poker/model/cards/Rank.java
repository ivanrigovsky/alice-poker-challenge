package rigovsky.ivan.poker.model.cards;

public enum Rank {

    queen("Queen", 13),
    king("King", 12),
    jack("Jack", 11),
    ten("10", 10),
    nine("9", 9),
    eight("8", 8),
    seven("7", 7),
    six("6", 6),
    five("5", 5),
    four("4", 4),
    three("3", 3),
    two("2", 2),
    ace("Ace", 1);

    private String title;
    private Integer value;

    Rank(String title, Integer value) {
        this.title = title;
        this.value = value;
    }

    public Integer getSize() {
        return values().length;
    }

    public Integer getValue() {
        return value;
    }

    public String toString() {
        return title;
    }
}
