package rigovsky.ivan.poker.model.cards;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import lombok.Data;

/**
 * Card deck.
 */

@Data
public class CardDeck {

    private final Integer DECK_SIZE = Rank.values().length * Suit.values().length;
    private List<Card> cards = new ArrayList<>(DECK_SIZE);

    public CardDeck() {
        initDeck();
    }

    /**
     * Shuffle card deck.
     */
    public void shuffle() {
        initDeck();
        Collections.shuffle(cards);
    }

    /**
     * Pull first card out from the deck.
     * @return pulled card.
     */
    public Card pullCard() {
        return cards.remove(0);
    }

    /**
     * Pull first cards out from the deck.
     * @param count cards count.
     * @return pulled cards.
     */
    public List<Card> pullCards(int count) {
        List<Card> result = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            result.add(pullCard());
        }
        return result;
    }

    /**
     * Clean and fill card deck.
     */
    private void initDeck() {
        cards.clear();
        Arrays.stream(Suit.values()).forEach(suit ->
            Arrays.stream(Rank.values()).forEach(
                rank -> cards.add(new Card(suit, rank))
            )
        );
        Collections.shuffle(cards);
    }
}
