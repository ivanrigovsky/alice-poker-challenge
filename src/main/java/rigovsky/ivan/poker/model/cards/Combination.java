package rigovsky.ivan.poker.model.cards;

public enum Combination {

    highCard("High card", 9),
    pair("One pair", 8),
    twoPair("Two pair", 7),
    set("Set", 6),
    straight("Straight", 5),
    flush("Flush", 4),
    fullHouse("Full house", 3),
    quads("Quads", 2),
    straightFlush("Straight flush", 1);

    private String title;
    private Integer rank;

    Combination(String title, Integer value) {
        this.title = title;
        this.rank = value;
    }

    public Integer getSize() {
        return values().length;
    }

    public Integer getRank() {
        return rank;
    }

    public String toString() {
        return title;
    }

}
