package rigovsky.ivan.poker.model.cards;

/**
 * Card suit.
 */
public enum Suit {

    diamonds("Diamonds"),
    clubs("Clubs"),
    hearts("Hearts"),
    spades("Spades");

    private String title;

    Suit(String title) {
        this.title = title;
    }

    public String toString() {
        return title;
    }
}
