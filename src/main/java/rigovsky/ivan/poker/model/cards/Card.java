package rigovsky.ivan.poker.model.cards;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Base card class.
 */
@Builder
@Data
@AllArgsConstructor
public class Card {

    private Suit suit;
    private Rank rank;

}
