package rigovsky.ivan.poker.model.game;

import java.util.List;
import lombok.Builder;
import lombok.Data;
import rigovsky.ivan.poker.model.cards.Card;
import rigovsky.ivan.poker.model.cards.Combination;
import rigovsky.ivan.poker.model.players.Player;

@Builder
@Data
public class WinnerInfo {

    private List<Card> cards;
    private Player player;
    private Long totalBank;
    private Combination combination;
}
