package rigovsky.ivan.poker.model.game;

/**
 * Command line game actions.
 */
public enum GameAction {

    FOLD(1),
    CHECK(2),
    RAISE(3),
    CALL(4);

    private int command;

    GameAction(int command) {
        this.command = command;
    }

    public int getCommand() {
        return command;
    }

    public static GameAction valueOf(int command) {
        switch (command) {
            case 1: return FOLD;
            case 2: return CHECK;
            case 3: return RAISE;
            case 4: return CALL;
            default: return null;
        }
    }
}
