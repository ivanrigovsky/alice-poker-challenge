package rigovsky.ivan.poker.model.players;

/**
 * Types of player.
 */
public enum PlayerType {

    robot("Robot"),
    human("Human");

    private String title;

    PlayerType(String title) {
        this.title = title;
    }

    public String toString() {
        return title;
    }
}
