package rigovsky.ivan.poker.model.players;

import java.util.List;
import lombok.Builder;
import lombok.Data;
import rigovsky.ivan.poker.bot.BotController;
import rigovsky.ivan.poker.model.cards.Card;

/**
 * Base player model with cards.
 */
@Data
public class Player {

    public Player(PlayerType type, BotController botController, int number, long money) {
        this.type = type;
        this.number = number;
        this.money = money;
        this.botController = botController;
    }
    /**
     * Cards on hand.
     */
    private List<Card> cards;

    /**
     * Number of player on the table.
     */
    private Integer number;

    /**
     * Player current money.
     */
    private Long money;

    /**
     * Player bet.
     */
    private Long bet;

    /**
     * Next after the dealer flag.
     */
    private boolean nextAfterDealer = false;

    /**
     * Player type - human or robot.
     */
    private PlayerType type;

    /**
     * Bot implementation to control game.
     */
    private BotController botController;

    public String getPlayerName() {
        return String.valueOf(number);
    }
}
