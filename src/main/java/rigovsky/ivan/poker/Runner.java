package rigovsky.ivan.poker;

import java.util.Scanner;
import java.util.stream.Collectors;
import rigovsky.ivan.poker.exception.GameException;
import rigovsky.ivan.poker.model.game.GameAction;
import rigovsky.ivan.poker.model.game.WinnerInfo;
import rigovsky.ivan.poker.model.players.PlayerType;
import rigovsky.ivan.poker.processor.GameInstance;
import rigovsky.ivan.poker.processor.GameProcessor;
import rigovsky.ivan.poker.processor.GameState;
import rigovsky.ivan.poker.processor.RulesConstants;

import static rigovsky.ivan.poker.model.game.GameAction.*;

/**
 * Poker Challenge runner. 
 */
public class Runner {

    private static Boolean ROBOT_DEBUG = Boolean.FALSE;

    public static void main(String[] args) {

        try {
            System.out.println("Welcome!");
            Scanner input = new Scanner(System.in);
            System.out.println("Please, enter number of players: ");
            int playersCount = getInteger(input, RulesConstants.PLAYERS_MIN, RulesConstants.PLAYERS_MAX);

            System.out.println("Please, enter big blind size in $: ");
            int bigBlind = getInteger(input, RulesConstants.BIG_BLIND_MINIMUM, null);

            System.out.println("Enter amount of money, for each player in $: ");
            int playerMoney = getInteger(input, bigBlind * RulesConstants.PLAYER_MIN_XBLIND_MONEY,
                bigBlind * RulesConstants.PLAYER_MAX_XBLIND_MONEY);

            GameProcessor game = new GameInstance();

            System.out.println("Poker Challenge started, enjoy!");
            game.initGame(playersCount, bigBlind, playerMoney);

            GameState state = game.setup();
            printState(state);

            //next round.
            while (state.activePlayers().size() > 1) {
                //bet or action by each player.
                while (state.currentPlayer() != null) {
                    try {
                        GameAction action;
                        if (!ROBOT_DEBUG && state.currentPlayer().getType().equals(PlayerType.robot)) {
                            action = state.currentPlayer().getBotController().getAction(state);
                        } else {
                            System.out.println("Please enter action number: (1) Fold (2) Check (3) Raise (4) Call.");
                            action = GameAction.valueOf(getInteger(input, FOLD.getCommand(), CALL.getCommand()));
                        }
                        state = getNextPlayerStep(action, input, game);
                        printState(state);
                    } catch (GameException e) {
                        System.out.println(e.getMessage());
                    }
                }

                //players in game, continue.
                if (state.winner() == null) {
                    System.out.println("-------------------");
                    System.out.println("Bets were made.\n");
                    state = getNextGameStep(state, game);
                    printState(state);
                }

                //next round init
                if (state.winner() != null) {
                    state = game.setup();
                    System.out.println("Please, press any key to continue.\n");
                    System.in.read();
                    System.out.println("*******************");
                    System.out.println("New round.\n");
                    printState(state);
                }
            }

            System.out.println("Game over. Thank you, for your time.");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Sorry, something went wrong. Please restart the application.");
        }
    }

    private static GameState getNextPlayerStep(GameAction action, Scanner input, GameProcessor game)
        throws GameException {
        if (action == null) {
            throw new IllegalStateException("Unexpected command.");
        }
        switch (action) {
            case FOLD:
                return game.fold();
            case CHECK:
                return game.check();
            case RAISE:
                System.out.println("Please enter bet amount in $");
                long betAmount = getInteger(input, RulesConstants.BIG_BLIND_MINIMUM, null);
                return game.raise(betAmount);
            case CALL:
                return game.call();
            default:
                throw new IllegalStateException("Unexpected command: " + action);
        }
    }

    private static GameState getNextGameStep(GameState state, GameProcessor game) throws GameException {
        if (state.activePlayers().size() == 1) {
            System.out.println("-------------------");
            System.out.println("Open cards.");
            return game.showdown();
        }
        if (state.cardsOnTable() == null) {
            System.out.println("-------------------");
            System.out.println("Flop");
            return game.flop();
        } else {
            switch (state.cardsOnTable().size()) {
                case RulesConstants.CARDS_FLOP:
                    System.out.println("-------------------");
                    System.out.println("Turn");
                    return game.turn();
                case RulesConstants.CARDS_TURN:
                    System.out.println("-------------------");
                    System.out.println("River");
                    return game.river();
                case RulesConstants.CARDS_RIVER:
                    System.out.println("-------------------");
                    System.out.println("Open cards.");
                    return game.showdown();
                default:
                    throw new IllegalStateException("Wrong number of cards on the table: "
                        + state.cardsOnTable().size());
            }
        }
    }

    private static int getInteger(Scanner input, Integer min, Integer limit) {
        Integer value = null;
        do {
            if (value != null) {
                if (min != null && limit != null) {
                    System.out.println(String.format("Please input value between %d and %d", min, limit));
                } else if (min != null) {
                    System.out.println(String.format("Please input value bigger than %d", min));
                } else {
                    System.out.println(String.format("Please input value smaller than %d", limit));
                }
            }
            try {
                value = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Please input numeric value");
                value = null;
            }
        } while (value == null || (min != null && value < min) || (limit != null && value > limit));
        return value;
    }

    private static void printState(GameState state) {
        System.out.println();
        if (state.cardsOnTable() != null) {
            String cardsOnTable = state.cardsOnTable().stream().map(card ->
                String.format("%s %s",
                    card.getRank().toString(),
                    card.getSuit().toString())
            ).collect(Collectors.joining(" | "));
            System.out.println(String.format("Table: cards %s, bet %d, bank %d", cardsOnTable, state.bet(),
                state.totalBank()));
        } else {
            System.out.println(String.format("Table: bet %d$, bank %d$", state.bet(), state.totalBank()));
        }

        String activePlayers = state.activePlayers().stream().map(player ->
            String.format("%s (%s) (%d$)",
                player.getPlayerName(),
                player.getType().toString(),
                player.getMoney())
        ).collect(Collectors.joining(" | "));
        System.out.println(String.format("Active players: %s", activePlayers));

        if (state.currentPlayer() != null) {
            String currentPlayerCards = "hidden cards";
            if (state.currentPlayer().getType() == PlayerType.human || ROBOT_DEBUG) {
                currentPlayerCards = state.currentPlayer().getCards().stream().map(card ->
                    String.format("%s %s",
                        card.getRank().toString(),
                        card.getSuit().toString())
                ).collect(Collectors.joining(" | "));
            }
            String currentPlayer = String.format("%s (%s) %d$ [%s]",
                    state.currentPlayer().getPlayerName(),
                    state.currentPlayer().getType().toString(),
                    state.currentPlayer().getMoney(),
                    currentPlayerCards);

            System.out.println(String.format("Current player: %s. You should add %d to finish blinds.",
                currentPlayer, state.bet() - state.currentPlayer().getBet()));
        }

        if (state.winner() != null) {
            WinnerInfo winner = state.winner();
            String winnerCards = winner.getCards().stream().map(card ->
                String.format("%s %s",
                    card.getRank().toString(),
                    card.getSuit().toString())
            ).collect(Collectors.joining(" | "));
            System.out.println(String.format("Winner: name: %s, cards %s, bank %d$",
                winner.getPlayer().getNumber(),
                winnerCards,
                state.totalBank()));

        }
    }
}
