package rigovsky.ivan.poker.processor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.Data;
import rigovsky.ivan.poker.model.cards.Card;
import rigovsky.ivan.poker.model.cards.Combination;
import rigovsky.ivan.poker.model.cards.Rank;
import rigovsky.ivan.poker.model.cards.Suit;

/**
 * Combinations calculator with card rank and suit.
 */
@Data
public class CombinationCalculator {

    private Combination combinationType;
    private Integer value = 0;
    private List<Card> winnerCombination = new ArrayList<>();
    private List<Card> tableCard;

    public List<Card> calculate(List<Card> cards) {
        winnerCombination = new ArrayList<>();
        tableCard = cards;

        //high card
        calcValue(cards, Combination.highCard);


        Map<Suit, List<Card>> cardsBySuits = cards.stream().collect(Collectors.groupingBy(Card::getSuit));
        Map<Rank, List<Card>> cardsByRanks = cards.stream().collect(Collectors.groupingBy(Card::getRank));
        Map<Integer, List<Card>> pairsByRank = new HashMap<>();
        Map<Integer, List<Card>> setsByRank = new HashMap<>();

        //flush
        cardsBySuits.forEach((key, values) -> {
            if (values.size() >= 5) {
                calcValue(values, Combination.flush);
            }
        });

        //quads
        cardsByRanks.forEach((key, values) -> {
            if (values.size() == 4) {
                calcValue(values, Combination.quads);
            } else {
                //pairs and sets combinations for full-house
                if (values.size() >= 2) {
                    pairsByRank.put(key.getValue(), values);
                }
                if (values.size() == 3) {
                    setsByRank.put(key.getValue(), values);
                }
            }
        });

        //Full house, set, pair, two-pair
        if (pairsByRank.size() >= 2 && setsByRank.size() >= 1) {
            Integer maxRankSet = Collections.max(setsByRank.keySet());
            pairsByRank.remove(maxRankSet);
            List<Card> fullHouseCards = setsByRank.get(maxRankSet);
            fullHouseCards.addAll(pairsByRank.get(Collections.max(pairsByRank.keySet())));
            calcValue(fullHouseCards, Combination.fullHouse);
        } else if (setsByRank.size() > 0) {
            Integer maxRankSet = Collections.max(setsByRank.keySet());
            calcValue(setsByRank.get(maxRankSet), Combination.set);
        } else if (pairsByRank.size() > 1) {
            List<Card> firstPairCards = pairsByRank.remove(Collections.max(pairsByRank.keySet()));
            List<Card> twoPairsCards = pairsByRank.get(Collections.max(pairsByRank.keySet()));
            twoPairsCards.addAll(firstPairCards);
            calcValue(twoPairsCards, Combination.twoPair);
        } else if (pairsByRank.size() > 0) {
            calcValue(pairsByRank.get(Collections.max(pairsByRank.keySet())), Combination.pair);
        }

        //TODO not implemented: Straight, Straight flash.

        return getWinnerCombination();
    }

    /**
     * Add additional cards to the winner combination.
     * @return 5 winner cards.
     */
    private List<Card> getWinnerCombination() {
        if (winnerCombination.size() < RulesConstants.CARDS_ON_THE_TABLE) {
            tableCard.sort(Comparator.comparing(Card::getRank));
            //remove combination cards from the table cards.
            tableCard.removeAll(winnerCombination);
            //add highest cards
            winnerCombination.addAll(tableCard.subList(0,
                RulesConstants.CARDS_ON_THE_TABLE - winnerCombination.size()));
        }
        return winnerCombination;
    }

    /**
     * Save combination value.
     * @param combinationCards cards with combination.
     * @param type new combination type.
     */
    private void calcValue(List<Card> combinationCards, Combination type) {
        if (combinationType != null && combinationType.getRank() < type.getRank())
            return;

        combinationCards.sort(Comparator.comparing(Card::getRank));
        if (combinationCards.size() >= 5) {
            combinationCards = combinationCards.subList(0, 5);
        }

        Integer newValue = combinationCards.stream()
            .map(card -> card.getRank().getValue())
            .mapToInt(value -> value)
            .sum();

        if (combinationType == null || combinationType.getRank() > type.getRank() || newValue > value) {
            value = newValue;
            combinationType = type;
            winnerCombination = combinationCards;
        }
    }
}
