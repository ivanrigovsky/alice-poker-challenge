package rigovsky.ivan.poker.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;
import rigovsky.ivan.poker.bot.BotController;
import rigovsky.ivan.poker.bot.SimpleBotController;
import rigovsky.ivan.poker.exception.GameException;
import rigovsky.ivan.poker.model.cards.Card;
import rigovsky.ivan.poker.model.cards.Combination;
import rigovsky.ivan.poker.model.game.WinnerInfo;
import rigovsky.ivan.poker.model.players.Player;
import rigovsky.ivan.poker.model.players.PlayerType;

/**
 * Main Poker Game processor.
 */
public class GameInstance implements GameState, GameProcessor {

    /**
     * Game settings.
     */
    private Long bigBlind;
    private Long smallBlind;

    /**
     * Players in the game.
     */
    private List<Player> players;
    private List<Player> activePlayers;
    private ListIterator<Player> activePlayerIterator;
    private Player currentPlayer;

    /**
     * Cards deck processor.
     */
    private DeckProcessor deckProcessor;

    /**
     * Table state
     */
    private List<Card> cardsOnTable;
    private Long totalBank = 0L;
    private Long currentBet = 0L;
    private boolean cardsOpened = false;

    /**
     * Winner information for final round.
     */
    private WinnerInfo winnerInfo = null;

    public void initGame(int playersCount, long blind, long playerMoney) {
        bigBlind = blind;
        smallBlind = bigBlind / 2;
        totalBank = 0L;
        players = new ArrayList<>(playersCount);
        for (int i = 0; i < playersCount; i++) {
            PlayerType type;
            BotController botController = null;
            if (i == 0) {
                type = PlayerType.human;
            } else {
                type = PlayerType.robot;
                botController = new SimpleBotController();
            }
            players.add(new Player(type, botController, i, playerMoney));
        }
        activePlayers = new ArrayList<>();
        deckProcessor = new DeckProcessor();
    }

    public GameState setup() throws GameException {
        validateStartGame();
        totalBank = 0L;
        winnerInfo = null;
        cardsOnTable = deckProcessor.getFlop();
        cardsOpened = false;

        //active players for new round.
        activePlayers = players.stream()
            .filter(player -> player.getMoney() >= bigBlind)
            .peek(player -> {
                player.setCards(deckProcessor.getPreFlop());
                player.setBet(0L);
            })
            .collect(Collectors.toList());

        //set next dealer.
        activePlayerIterator = activePlayers.listIterator();
        currentPlayer = activePlayerIterator.next();
        while (activePlayerIterator.hasNext() && !currentPlayer.isNextAfterDealer()) {
            currentPlayer = activePlayerIterator.next();
        }
        if (!currentPlayer.isNextAfterDealer()) {
            currentPlayer.setNextAfterDealer(true);
        }

        //current bet
        currentBet = bigBlind;

        //set small blind
        getNextBlind(smallBlind);

        //set big blind
        getNextBlind(bigBlind);

        nextPlayer();
        return this;
    }

    public GameState check() throws GameException {
        validateSetup();

        //pay more
        long needToPay = currentBet - currentPlayer.getBet();
        if (needToPay > 0) {
            if (currentPlayer.getMoney() < needToPay) {
                //no money, remove.
                activePlayerIterator.remove();
            } else {
                //added money.
                currentPlayer.setMoney(currentPlayer.getMoney() - needToPay);
                currentPlayer.setBet(currentPlayer.getBet() + needToPay);
                totalBank += needToPay;
            }
        }
        checkLastBet();
        return this;
    }

    public GameState call() throws GameException {
        return check();
    }

    public GameState fold() throws GameException {
        validateSetup();
        //fold, remove player.
        if (activePlayers.size() > 1) {
            activePlayerIterator.remove();
            nextPlayer();
        }
        //open card for the last player.
        if (activePlayers.size() == 1) {
            Player winner = activePlayers.get(0);
            winnerInfo = WinnerInfo.builder()
                .cards(winner.getCards())
                .player(winner)
                .combination(Combination.highCard)
                .totalBank(totalBank)
                .build();
            finishRound(winner);
            currentPlayer = null;
        }
        return this;
    }

    public GameState flop() throws GameException {
        validateSetup();
        cardsOpened = true;
        nextPlayer();
        return this;
    }

    public GameState turn() throws GameException {
        validateSetup();
        cardsOnTable.add(deckProcessor.getTurn());
        nextPlayer();
        return this;
    }

    public GameState river() throws GameException {
        validateSetup();
        cardsOnTable.add(deckProcessor.getRiver());
        nextPlayer();
        return this;
    }

    public GameState showdown() throws GameException {
        CombinationCalculator combinationCalculator = new CombinationCalculator();

        if (!cardsOpened) {
            flop();
        } else if (cardsOnTable.size() < RulesConstants.CARDS_TURN) {
            turn();
        } else if (cardsOnTable.size() < RulesConstants.CARDS_RIVER) {
            river();
        }

        validateBettingRounds();
        Integer winnerValue = 0;
        Player winner = null;
        Combination winnerCombination = Combination.highCard;
        List<Card> winnerCards = cardsOnTable;

        for (Player player : activePlayers) {
            List<Card> playerCards = new ArrayList<>(player.getCards());
            playerCards.addAll(cardsOnTable);
            List<Card> combinationCards = combinationCalculator.calculate(playerCards);
            //select winner
            if (combinationCalculator.getCombinationType().getRank() <= winnerCombination.getRank()
                && combinationCalculator.getValue() >= winnerValue) {
                winnerCards = new ArrayList<>(combinationCards);
                winnerCombination = combinationCalculator.getCombinationType();
                winnerValue = combinationCalculator.getValue();
                winner = player;
            }
        }

        winnerInfo = WinnerInfo.builder()
            .combination(winnerCombination)
            .player(winner)
            .totalBank(totalBank)
            .cards(winnerCards)
            .build();

        finishRound(winner);

        return this;
    }

    public GameState raise(long amount) throws GameException {
        validateSetup();
        validateBet(amount);
        currentPlayer.setMoney(currentPlayer.getMoney() - amount);
        currentPlayer.setBet(currentPlayer.getBet() + amount);
        currentBet = currentPlayer.getBet();
        totalBank += amount;

        checkLastBet();
        return this;
    }

    public List<Player> activePlayers() {
        return activePlayers;
    }

    public List<Card> cardsOnTable() {
        if (!cardsOpened) {
            return null;
        }
        return cardsOnTable;
    }

    public Player currentPlayer() {
        return currentPlayer;
    }

    public Long bet() {
        return currentBet;
    }

    public Long totalBank() {
        return totalBank;
    }

    public WinnerInfo winner() {
        return winnerInfo;
    }

    /**
     * Clean all table state fields.
     */
    private void finishRound(Player winner) {
        if (winner != null) {
            winner.setMoney(winner.getMoney() + totalBank);
        }
        players.forEach(player -> {
            player.getCards().clear();
        });
        activePlayers.clear();
    }

    /**
     * Check if current player did check or call.
     */
    private void checkLastBet() {
        nextPlayer();
        //if next player has the same bet.
        if (currentPlayer.getBet().equals(currentBet)) {
            //finished all bets.
            currentPlayer = null;
        }
    }

    /**
     * Get next player for blind bet.
     * @param amount small or big blind.
     */
    private void getNextBlind(long amount) {
        nextPlayer();
        while (currentPlayer.getMoney() < amount) {
            activePlayerIterator.remove();
            nextPlayer();
        }
        currentPlayer.setMoney(currentPlayer.getMoney() - amount);
        currentPlayer.setBet(amount);
        totalBank += amount;
    }

    private void nextPlayer() {
        if (activePlayerIterator.hasNext()) {
            currentPlayer = activePlayerIterator.next();
        } else if (activePlayers.size() > 0) {
            activePlayerIterator = activePlayers.listIterator();
            currentPlayer = activePlayerIterator.next();
        }
    }

    private void validateBet(long amount) throws GameException {
        if (amount < bigBlind) {
            throw new GameException(String.format("Wrong blind amount: %d. Should be mmore than %d", amount, bigBlind));
        }
        if (currentBet > (amount + currentPlayer.getBet())) {
            throw new GameException("You should set bet more than " + (currentBet - currentPlayer.getBet()));
        }
        if (currentPlayer.getMoney() < amount) {
            throw new GameException("You don't have money for the bet");
        }
    }

    private void validateBettingRounds() throws GameException {
        validateStartGame();
        if (cardsOnTable.size() != RulesConstants.CARDS_ON_THE_TABLE && activePlayers.size() == 0) {
            throw new GameException("Game is not ready for showdown.");
        }
    }

    private void validateSetup() throws GameException {
        validateStartGame();
        if (activePlayerIterator == null || (cardsOnTable == null || cardsOnTable.size() == 0)) {
            throw new GameException("Game is not ready.");
        }
    }

    private void validateStartGame() throws GameException {
        if (deckProcessor == null || activePlayers == null) {
            throw new GameException("Game is not started.");
        }
    }

}
