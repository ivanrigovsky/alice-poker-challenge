package rigovsky.ivan.poker.processor;

import java.util.List;
import rigovsky.ivan.poker.model.cards.Card;
import rigovsky.ivan.poker.model.game.WinnerInfo;
import rigovsky.ivan.poker.model.players.Player;

/**
 * Current state game information.
 */
public interface GameState {

    /**
     * Show active players.
     * @return active players.
     */
    List<Player> activePlayers();

    /**
     * Show cards on table.
     * @return cards on table.
     */
    List<Card> cardsOnTable();

    /**
     * Show current player.
     * @return current player.
     */
    Player currentPlayer();

    /**
     * Current bet;
     */
    Long bet();

    /**
     * Current cash on the table.
     */
    Long totalBank();

    /**
     * Winner information.
     */
    WinnerInfo winner();
}

