package rigovsky.ivan.poker.processor;

import java.util.List;
import rigovsky.ivan.poker.model.cards.Card;
import rigovsky.ivan.poker.model.cards.CardDeck;

public class DeckProcessor {

    private CardDeck cardDeck;

    public DeckProcessor() {
        cardDeck = new CardDeck();
    }

    /**
     * Shuffle and get the first three community cards dealt.
     */
    public List<Card> getFlop() {
        cardDeck.shuffle();
        return cardDeck.pullCards(RulesConstants.CARDS_FLOP);
    }

    /**
     * Anything that occurs before the flop is dealt is preflop.
     */
    public List<Card> getPreFlop() {
        return cardDeck.pullCards(RulesConstants.CARDS_PRE_FLOP);
    }

    /**
     * The final (5th) community card dealt; also known as fifth street.
     */
    public Card getRiver() {
        return cardDeck.pullCard();
    }

    /**
     * The fourth community card dealt; also known as fourth street
     */
    public Card getTurn() {
        return cardDeck.pullCard();
    }

    /**
     * Get cards.
     * @return cards list.
     */
    public List<Card> getAllCards() {
        return cardDeck.getCards();
    }
}
