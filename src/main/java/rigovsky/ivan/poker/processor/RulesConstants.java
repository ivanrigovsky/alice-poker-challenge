package rigovsky.ivan.poker.processor;

/**
 * Public Poker rules constant values.
 */
public class RulesConstants {

    public static final int BIG_BLIND_MINIMUM = 2;
    public static final int PLAYERS_MIN = 2;
    public static final int PLAYERS_MAX = 10;
    public static final int CARDS_PRE_FLOP = 2;
    public static final int CARDS_FLOP = 3;
    public static final int CARDS_TURN = 4;
    public static final int CARDS_RIVER = 5;
    public static final int CARDS_ON_THE_TABLE = 5;
    public static final int PLAYER_MIN_XBLIND_MONEY = 20;
    public static final int PLAYER_MAX_XBLIND_MONEY = 100;
}
