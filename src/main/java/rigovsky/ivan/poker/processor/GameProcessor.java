package rigovsky.ivan.poker.processor;

import rigovsky.ivan.poker.exception.GameException;

/**
 * Game processing methods.
 */
public interface GameProcessor {

    /**
     * Iinit settings and first step.
     * @param playersCount number of players.
     * @param bigBlind big blind in $.
     * @param playerMoney player money.
     */
    void initGame(int playersCount, long bigBlind, long playerMoney);

    /**
     * Set blinds, get cards.
     */
    GameState setup() throws GameException;

    /**
     * Similar to a call but no money is bet. If there is no raise preflop, the big blind may check.
     */
    GameState check() throws GameException;

    /**
     * Match the amount of the big blind (pre-flop this is also known as "limping in.").
     */
    GameState call() throws GameException;

    /**
     * Pay nothing to the pot and throw away their hand, waiting for the next deal to play again.
     */
    GameState fold() throws GameException;

    /**
     * Once the preflop betting round ends, the flop is dealt. This is done by dealing the top card in the deck
     * facedown on the table (called the "burn" card, it's not in play),
     * followed by three cards face-up in the middle of the table (see below).
     */
    GameState flop() throws GameException;

    /**
     * Once the betting round on the flop completes (meaning any players who want to see the next card have matched
     * the value of any bets), the dealer again 'burns" one card facedown out of play followed by a single card face-up
     * in the middle of the table beside the 3 flop cards (see image below).
     */
    GameState turn() throws GameException;

    /**
     * Assuming more than one player is left having not folded on one of the previous streets, the river is now dealt.
     * Dealing the river is identical as dealing the turn with one card being burned facedown followed by
     * a single card face-up.
     */
    GameState river() throws GameException;

    /**
     * When players reveal their hands to discover the pot's winner.
     */
    GameState showdown() throws GameException;

    /**
     * Raise the bet by doubling the amount of the big blind.
     * (Note:  a player may raise more depending on the betting style being played, again see the rules for No-Limit and Pot-Limit above.)
     */
    GameState raise(long amount) throws GameException;

}
