package rigovsky.ivan.poker.bot;

import rigovsky.ivan.poker.model.game.GameAction;
import rigovsky.ivan.poker.processor.GameState;

/**
 * Bot logic controller with simple 'always-call- logic.
 */
public class SimpleBotController implements BotController {

    /**
     * Get next action by game state.
     * @param state game state.
     * @return next action.
     */
    public GameAction getAction(GameState state) {
        //always call simple strategy.
        return GameAction.CALL;
    }

}
