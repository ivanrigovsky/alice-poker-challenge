package rigovsky.ivan.poker.bot;

import rigovsky.ivan.poker.model.game.GameAction;
import rigovsky.ivan.poker.processor.GameState;

/**
 * Bot logic interface.
 */
public interface BotController {

    GameAction getAction(GameState state);
}
