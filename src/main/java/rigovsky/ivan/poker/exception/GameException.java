package rigovsky.ivan.poker.exception;

/**
 * Base game exception.
 */
public class GameException extends Exception {

    public GameException(String message) {
        super(message);
    }
}
